class DashboardController < ApplicationController
  unloadable
  include DashboardHelper

  def index
    if (params[:project_id]) then
      @project = Project.find(params[:project_id])
      if(params[:tracker] && params[:tracker] != '0' ) then
        @currentTracker = @project.trackers.where('id = ?', params[:tracker]).first
      else
        @currentTracker = Tracker.new(:id => 0, :name => 'All')
      end
    else
      @project = Project.new(:id => 0, :name => "ensembliste", :description => "Ensembles des projets") 
      if(params[:tracker] && params[:tracker] != '0') then
        @currentTracker = Tracker.where('id = ?', params[:tracker]).first
      else
    		@currentTracker = Tracker.new(:id => 0, :name => 'All')
      end
    end

    @trackers_to_select = Array.new
    @trackers_to_select << ['All', 0]
    @project.trackers.each do |t|
      @trackers_to_select << [t.name, t.id]
    end

    if(params[:startDate]!=nil && params[:startDate] != '') then
      @startDate = params[:startDate].to_date
    else
      @startDate = 3.months.ago.to_date
    end
    if(params[:endDate]!=nil && params[:endDate] !='') then
      @endDate = params[:endDate].to_date
    else
      @endDate = Date.today
    end

    @subProjects = getSubProjects @project
  end
end

