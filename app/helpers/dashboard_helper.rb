module DashboardHelper

  def getAllRecursiveSubProjects(projects)
    subprojects = []
    projects.each do | project |
      subprojects = subprojects.concat Project.where(:parent_id => project.id)
    end
    subprojects = subprojects.concat getAllRecursiveSubProjects(subprojects) if (!subprojects.empty?)
    subprojects
  end
  
  def getSubProjects project
    if(project.id == 0 || project.id == nil) then
      Project.where(:parent_id => nil)
    else
      Project.where(:parent_id => project.id)
    end
  end
  
  def getRecursiveIssues(project, tracker, startDate, endDate)
    if (project.id == 0 || project.id == nil ) then
      issues = Issue.where(:tracker_id => tracker.id, :created_on => startDate..endDate+1) if (tracker.id != nil)
      issues = Issue.where(:created_on => startDate..endDate) if (tracker.id == nil)
    else
      issues = project.issues.where(:tracker_id => tracker.id, :created_on => startDate..endDate+1) if (tracker.id != nil)
      issues = project.issues.where(:created_on => startDate..endDate+1) if (tracker.id == nil)
      allSubProjects = getAllRecursiveSubProjects([project])
      allSubProjects.each do |p|
        issues = issues.concat p.issues.where(:tracker_id => tracker.id, :created_on => startDate..endDate+1) if (tracker.id != nil && p.issues.size > 0)
        issues = issues.concat p.issues.where(:created_on => startDate..endDate+1) if (tracker.id == nil && p.issues.size > 0)
      end
    end
    puts 'getRecursiveIssues'
    issues
  end

  def getIssues (project, tracker, startDate, endDate)
    if (project.id == 0 || project.id == nil ) then
      issues = nil
    else
      issues = Issue.where(:project_id => project.id, :tracker_id => tracker.id, :created_on => startDate..endDate+1) if (tracker.id != nil)
      issues = Issue.where(:project_id => project.id, :created_on => startDate..endDate+1) if (tracker.id == nil)
    end
    puts 'getIssues'
    issues  
  end

  def getIssuesStatus project, tracker, startDate, endDate
    tabStatusIssues = []
    issues = getRecursiveIssues(project, tracker, startDate, endDate)
    issues.each do |issue|
      findIt = false
      tabStatusIssues.each do | hash |
        if(hash.has_key?(:label) && hash[:label] == issue.status.name) then
          hash[:value] = hash[:value] + 1
          findIt = true
        end
      end
      if(!findIt) then tabStatusIssues.push label: issue.status.name, value: 1 end
    end
    tabStatusIssues
  end

  def getIssuesPriority project, tracker, startDate, endDate
    tabPriorityIssues = []
    issues = getRecursiveIssues(project, tracker, startDate, endDate)
    issues.each do |issue|
      findIt = false
      tabPriorityIssues.each do | hash |
        if(hash.has_key?(:label) && hash[:label] == issue.priority.name) then
          hash[:value] = hash[:value] + 1
          findIt = true
        end
      end
      if(!findIt) then tabPriorityIssues.push label: issue.priority.name, value: 1 end
    end
    tabPriorityIssues
  end

  def getIssuesEvolution project, tracker, startDate, endDate
    tabIssuesEvolution = []
    tmp = 1
    issues = getRecursiveIssues(project, tracker, startDate, endDate)
    issues.each do |issue|
      if(!issue.status.is_closed) then
        findIt = false
        tabIssuesEvolution.each do | hash |
          if(hash.has_key?(:date) && hash[:date].to_date.to_s == issue.created_on.to_date.to_s) then
            hash[:value] = hash[:value] + 1
            findIt = true
          end
        end
        if(!findIt) then tabIssuesEvolution.push date: issue.created_on.to_date.to_s, value: tmp end
      end
      tmp = tmp+1
    end
    tabIssuesEvolution
  end

  def numberOfIssues project, tracker, startDate, endDate
     getRecursiveIssues(project, tracker, startDate, endDate).size
  end

  def numberAndPartOfOpenedIssues project, tracker, startDate, endDate
    i=0
    iOpen=0
    getRecursiveIssues(project, tracker, startDate, endDate).each do |issue|
      i=i+1
      iOpen=iOpen+1 if !issue.status.is_closed
    end
    i=1 if(i==0)
    iOpen.to_s  + ' (' + (iOpen*100/i).to_s + '%)'
  end

  def mostPriority project, currentTracker, startDate, endDate
    issuesPriority = getIssuesPriority(project, currentTracker, startDate, endDate)
    n=0
    tmp = issuesPriority.first
    issuesPriority.each do |hash|
      if (hash[:value]>n) then
        tmp = hash
        n=hash[:value] 
      end
    end
    if(tmp != nil) then
      tmp[:label].to_s
    end

  end

  def mostStatus project, currentTracker, startDate, endDate
    issuesStatus = getIssuesStatus(project, currentTracker, startDate, endDate)
    n=0
    tmp = issuesStatus.first
    issuesStatus.each do |hash|
      if (hash[:value]>n) then
        tmp = hash
        n=hash[:value] 
      end
    end
    if(tmp != nil) then
      tmp[:label].to_s
    end

  end

  def averageTheoricalDelay project, currentTracker, startDate, endDate
    issues = getRecursiveIssues(project, currentTracker, startDate, endDate)
    delay=0
    i=0
    issues.each do |issue|
      if(issue.due_date != nil) then
        delay = delay + (issue.due_date.to_date - issue.start_date.to_date).to_i
        i=i+1
      end
    end
    i=1 if (i==0)
    (delay/i).to_s + " jour(s)"
  end

  def partPastDue project, currentTracker, startDate, endDate
    issues = getRecursiveIssues(project, currentTracker, startDate, endDate)
    n = issues.size
    i = 0
    issues.each do |issue|
      if(issue.due_date!=nil) then
        if(issue.status.is_closed && issue.updated_on.to_date>issue.due_date.to_date) then
          i=i+1
        elsif(!issue.status.is_closed && Date.today>issue.due_date.to_date)
          i=i+1
        end
      end
    end
    n=1 if (n==0)
    i.to_s + " (" + (i*100/n).to_s + "%)"
  end

  def resolved? issue
    nbDaysResolved = (issue.updated_on.to_date-issue.created_on.to_date).to_i
    nbDaysNotResolved = (Date.today - issue.created_on.to_date).to_i
    if issue.status.is_closed && nbDaysResolved >= 30 then
      "<td bgcolor=#d9edf7>".html_safe+"Resolu en " + nbDaysResolved.to_s + " jour(s)"+"</td>".html_safe
    elsif issue.status.is_closed
      "<td bgcolor=#dff0d8>".html_safe+"Resolu en " + nbDaysResolved.to_s + " jour(s)"+"</td>".html_safe
    elsif nbDaysNotResolved >= 30
      "<td bgcolor=#f2dede>".html_safe+"Non resolu depuis " + nbDaysNotResolved.to_s + " jour(s)"+"</td>".html_safe
    else
      "<td>".html_safe+"Non resolu depuis " + nbDaysNotResolved.to_s + " jour(s)"+"</td>".html_safe
    end
  end

  def deadline? issue
    if issue.due_date==nil then
      -1
    elsif issue.status.is_closed && issue.updated_on.to_date>issue.due_date.to_date
      1
    elsif !issue.status.is_closed && Date.today>issue.due_date.to_date
      1
    else
      0
    end    
  end

end