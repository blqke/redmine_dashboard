jQuery(document).ready(function($) {
	if ($('#issues-status').length) {
		Morris.Donut({
			element: 'issues-status',
			data: $('#issues-status').data('list'),
			formatter: function(x) { 
				(x>1) ? text = x + " demandes" : text = x + " demande";
				return text;
			}
		});
	}
	if ($('#issues-priority').length) {
		Morris.Donut({
			element: 'issues-priority',
			data: $('#issues-priority').data('list'),
			formatter: function(x) { 
				(x>1) ? text = x + " demandes" : text = x + " demande";
				return text;
			},
			// colors: [
			// 	'#D23600',
			// 	'#D95100',
			// 	'#DE6D00',
			// 	'#EE8900',
			// 	'#FCA600'
			// ]
		});
	}
	if ($('#issues-evolution').length) {
		Morris.Area({
			element: 'issues-evolution',
			data: $('#issues-evolution').data('list'),
			xkey: 'date',
			ykeys: ['value'],
			labels: ['Evolution des demandes (ouvertes) sur une periode donnée']
		});
	}
});