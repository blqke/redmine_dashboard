Redmine::Plugin.register :dashboard do
  name 'Dashboard plugin'
  author 'Jules Libert'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'

  # menu :application_menu, :dashboard, { :controller => 'dashboard', :action => 'index' }, :caption => 'dashboard'

  permission :dashboard, { :dashboard => [:index] }, :public => true
  menu :project_menu, :dashboard, {:controller => 'dashboard', :action => 'index'}, :caption => 'Tableau de bord', :first => :true, :param => :project_id
  menu :application_menu, :dashboard, {:controller => 'dashboard', :action => 'index'}, :caption => 'Tableau de bord', :first => :true

end
